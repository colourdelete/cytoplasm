package ui

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gookit/color"
	terminal "github.com/wayneashleyberry/terminal-dimensions"
)

var msgTypeMap = map[string][2]color.Color{
	"success": {color.BgGreen, color.BgDefault},
	"info": {color.BgBlue, color.BgDefault},
	"warning": {color.BgYellow, color.BgDefault},
	"error": {color.BgYellow.Darken(), color.BgDefault},
	"critical": {color.BgRed, color.BgDefault},
	"debug": {color.BgGray, color.BgDefault},
}

var bf = bufio.NewWriter(os.Stdout)

// PrintProgress is a function to print a progress bar in the current line. It deletes any content on the same line.
func PrintProgress(progressCurrent, progressMax int, msg string, msgType string) error {
	var colorA = msgTypeMap[msgType][0]
	var colorB = msgTypeMap[msgType][1]
	var progress = float64(progressCurrent) / float64(progressMax)
	progress /= 2
	msg = fmt.Sprintf("\r%4v", fmt.Sprint(progress*200) + "%") + " " + msg
	var termWidth, err = terminal.Width()
	if err != nil {
		return err
	}
	var msgCut = int(float64(termWidth) * progress)
	var msgA, msgB string
	if len(msg) > msgCut {
		msgA = msg[:msgCut]
		msgB = msg[msgCut:]
	} else {
		msgA = msg + strings.Repeat(" ", msgCut-len(msg))
		msgB = strings.Repeat(" ", msgCut)
	}
	colorA.Print(msgA)
	colorB.Print(msgB)
	time.Sleep(1 * time.Millisecond) // Provide a chance to flush console buffer
	return bf.Flush()
}

// PrintlnProgress is a function to add a newline at the end of a PrintProgress.
func PrintlnProgress(progressCurrent, progressMax int, msg, msgType string) error {
	var result = PrintProgress(progressCurrent, progressMax, msg, msgType)
	msgTypeMap[msgType][1].Println()
	return result
}
