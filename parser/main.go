package parser

import (
	"io"
	"github.com/alecthomas/participle"
)

// Parse is a function to parse an io.Reader and convert it into an *AST and an error.
func Parse(src io.Reader) (*AST, error) {
	var ast = &AST{}
	parser, err := participle.Build(&AST{},
		participle.Lexer(iniLexer),
	)
	if err != nil {
		return nil, err
	}
	err = parser.Parse(src, ast)
	if err != nil {
		return nil, err
	}
	return ast, nil
}
